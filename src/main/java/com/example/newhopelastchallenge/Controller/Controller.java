package com.example.newhopelastchallenge.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    @FXML
    private Button buttonLogin;

    @FXML
    private Button toSignUp;

    @FXML
    private TextField emailInput;

    @FXML
    private TextField passInput;

//    @FXML
//    private TextField nickname;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        buttonLogin.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                db.loginUser(event, emailInput.getText(), passInput.getText(), null);
//                try {
////                    messengerServerController.runServer();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }

            }
        });

        toSignUp.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                db.changePage(event, "SignUp.fxml", "fgdgf", emailInput.getText(), passInput.getText(), null);
            }
        });


//    public void handle(ActionEvent event){
//        db.loginUser(event, emailInput.getText(), passInput.getText());
//    }
//


    }
};
