package com.example.newhopelastchallenge.Controller;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

public class signUpController implements Initializable {
//    @FXML
//    private Button buttonLogin;

    @FXML
    private Button buttonSignUp;

    @FXML
    private TextField emailInput;

    @FXML
    private TextField passInput;

    @FXML
    private TextField nickname;

    @Override
    public void initialize(URL location, ResourceBundle resourceBundle) {
        buttonSignUp.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                if (!emailInput.getText().trim().isEmpty() && !passInput.getText().trim().isEmpty()) {
                    System.out.println("new user");
//                    try {
//                        messengerServerController.runServer();
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                    db.signUp(event, emailInput.getText(), passInput.getText(), nickname.getText());
                }
                    else {
                    System.out.println("Not filled");
                    Alert alert = new Alert(Alert.AlertType.ERROR);
                    alert.setContentText("Please fill in all fields signup page");
                }
            }
        });

//        buttonLogin.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent event) {
//                db.changePage(event, "welcomePage.fxml", "Log in!", null, null, null);
//            }
//        });
    }
}