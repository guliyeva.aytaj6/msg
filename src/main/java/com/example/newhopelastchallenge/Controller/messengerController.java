package com.example.newhopelastchallenge.Controller;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.NodeOrientation;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;

public class messengerController extends Thread implements Initializable {


    @FXML
    private Button button_send;
    @FXML
    private TextField tf_message;
    @FXML
    private VBox vbox_messages;
    @FXML
    private ScrollPane sp_main;

    @FXML
    private Label usernameLabel;

    BufferedReader reader;
    PrintWriter writer;
    Socket socket;


//    public void connectSocket() {
//        try {
//            socket = new Socket("localhost", 8889);
//            System.out.println("Socket is connected with server!");
//            reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
//            writer = new PrintWriter(socket.getOutputStream(), true);
//            this.start();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }


    public void run(String nickname) {
        try {
            while (true) {
                String msg = reader.readLine();
                String[] tokens = msg.split(" ");
                String cmd = tokens[0];
                System.out.println(cmd);
                StringBuilder fulmsg = new StringBuilder();
                for (int i = 1; i < tokens.length; i++) {
                    fulmsg.append(tokens[i]);
                }
                System.out.println(fulmsg);
                if (cmd.equalsIgnoreCase(nickname + ":")) {
                    continue;
                } else if (fulmsg.toString().equalsIgnoreCase("bye")) {
                    break;
                }
                vbox_messages.setAccessibleText(msg + "\n");
//                        appendText(msg + "\n");
            }
            reader.close();
            writer.close();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//
//    public void handleSendEvent(MouseEvent event) {
//        send();
//        for(User user : users) {
//            System.out.println(user.name);
//        }
//    }


//    public void send(String nickname) {

//        button_send.setOnAction(new EventHandler<ActionEvent>() {
//            @Override
//            public void handle(ActionEvent actionEvent) {
//                String msg = tf_message.getText();
//                writer.println(nickname + ": " + msg);
//                tf_message.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
//                tf_message.appendText("Me: " + msg + "\n");
//                tf_message.setText("");
//                if (msg.equalsIgnoreCase("BYE") || (msg.equalsIgnoreCase("logout"))) {
//                    System.exit(0);
//                }
//            }
//        });
//    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        /*        usernameLabel.setText(nickname);*/
//        connectSocket();

        button_send.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                String msg = tf_message.getText();
                writer.println( ": " + msg);
                tf_message.setNodeOrientation(NodeOrientation.LEFT_TO_RIGHT);
                tf_message.appendText("Me: " + msg + "\n");
                tf_message.setText("");
                if (msg.equalsIgnoreCase("BYE") || (msg.equalsIgnoreCase("logout"))) {
                    System.exit(0);
                }
            }
        });

    }
}