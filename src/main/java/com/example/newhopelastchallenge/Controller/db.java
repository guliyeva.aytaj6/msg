package com.example.newhopelastchallenge.Controller;


import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

import java.sql.*;

public class db {


    public static void changePage(ActionEvent event, String fxmlFile, String title, String email, String password, String nickname) {
        Parent root = null;

        if (email != null && password != null) {
            try {
                FXMLLoader loader = new FXMLLoader(db.class.getResource(fxmlFile));
                root = loader.load();
//                messengerController messengerController = loader.getController();
//                messengerController.setUserNickname(nickname);

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                root = FXMLLoader.load(db.class.getResource(fxmlFile));

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle(title);
        stage.setScene(new Scene(root, 600, 400));
        stage.show();

    }


    public static void main(String args[]) {

        try {


            Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/Messenger", "root", "CarpeDiem666.7E");
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from Messenger.User");

            while (resultSet.next()) {
                System.out.println(resultSet.getString("password"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public static void signUp(ActionEvent event, String emailInput, String passInput, String nickname) {

        Connection connection = null;
        PreparedStatement preparedStatement = null;
        PreparedStatement isUserExist = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/Messenger", "root", "CarpeDiem666.7E");
            isUserExist = connection.prepareStatement("select * from Messenger.User where email = ?");
            isUserExist.setString(1, emailInput);
            resultSet = isUserExist.executeQuery();

            if (resultSet.isBeforeFirst()) {
                System.out.println("This email already registered");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("This email already registered");
                alert.show();
            } else {
                preparedStatement = connection.prepareStatement("INSERT INTO Messenger.User (email, password, nickname)" + "values (?, ?, ?)");
                preparedStatement.setString(1, emailInput);
                preparedStatement.setString(2, passInput);
                preparedStatement.setString(3, nickname);
                preparedStatement.executeUpdate();
                System.out.println("open messenger");
                changePage(event, "finalMessenger.fxml", "Welcome on the chat", emailInput, passInput, nickname);



            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (isUserExist != null) {

                try {
                    isUserExist.close();
                } catch (SQLException e) {

                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {

                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        }
    }



    public static void loginUser(ActionEvent event, String emailInput, String passInput, String nickname) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;

        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3307/Messenger", "root", "CarpeDiem666.7E");
            preparedStatement = connection.prepareStatement("select password, nickname from Messenger.User where email = ?");
            preparedStatement.setString(1, emailInput);


            if (resultSet.isBeforeFirst()) {
                System.out.println("This accaunt doesn`t exist");
                Alert alert = new Alert(Alert.AlertType.ERROR);
                alert.setContentText("Provided data is not correct!");
                alert.show();
            } else {
                while (resultSet.next()) {
                    String receivedPass = resultSet.getString("password");
                    String receivedNickname = resultSet.getString("nickname");

                    if (receivedPass.equals(passInput)) {
                        changePage(event, "finalMessenger.fxml", "Welcome!", emailInput, passInput, receivedNickname);
                    } else {
                        System.out.println("Password didnt match");
                        Alert alert = new Alert(Alert.AlertType.ERROR);
                        alert.setContentText("Provided data is not correct!");
                        alert.show();
                    }


                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();

                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {

                    e.printStackTrace();
                }
            }

            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        }
    }

}
